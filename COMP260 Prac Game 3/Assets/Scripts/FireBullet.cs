﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	public float reload = .2f;
	private float shoottimer = 0f;

	void Start () {
	}

	void Update () {
		//countdown timer
		shoottimer -= Time.deltaTime;
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1") && shoottimer <= 0.0f) {
			Fire ();
		}
	}
	void Fire () {
		BulletMove bullet = Instantiate(bulletPrefab);
		// the bullet starts at the player's position
		bullet.transform.position = transform.position;

		// create a ray towards the mouse location
		Ray ray = 
			Camera.main.ScreenPointToRay(Input.mousePosition);
		bullet.direction = ray.direction;

		//reset reload;
		shoottimer = reload;
	}
}
